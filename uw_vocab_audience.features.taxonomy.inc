<?php

/**
 * @file
 * uw_vocab_audience.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function uw_vocab_audience_taxonomy_default_vocabularies() {
  return array(
    'uwaterloo_audience' => array(
      'name' => 'Audience',
      'base_i18n_mode' => 4,
      'base_language' => 'und',
      'machine_name' => 'uwaterloo_audience',
      'description' => 'Who does this content apply to?',
      'hierarchy' => 0,
      'module' => 'uw_vocab_audience',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 4,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
