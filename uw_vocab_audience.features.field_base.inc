<?php

/**
 * @file
 * uw_vocab_audience.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function uw_vocab_audience_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_term_lock'.
  $field_bases['field_term_lock'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_term_lock',
    'field_permissions' => array(
      'type' => 2,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        2 => 'Unlocked',
        1 => 'Locked',
      ),
      'allowed_values_function' => '',
      'entity_translation_sync' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'list_boolean',
  );

  // Exported field_base: 'taxonomy_synonym'.
  $field_bases['taxonomy_synonym'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'description' => 'Synonyms of this term',
    'entity_types' => array(),
    'field_name' => 'taxonomy_synonym',
    'field_permissions' => array(),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'label' => 'Synonyms',
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  return $field_bases;
}
